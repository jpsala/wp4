var webpack = require('webpack');
var path = require("path");
let cfg = {
    context: __dirname,
    entry: {
        main: './src/main.js',
    },
    output: {
        path: path.join(__dirname, "dist"),
        filename: "[name].bundle.js",
        publicPath: 'http://localhost:8080/dist'
    },
    devtool: 'eval',
    module: {
        preLoaders: [
            {test: /\.js$/, loader: "eslint-loader", exclude: /node_modules/}
        ],
        loaders: [
            {test: /\.js$/, loaders: ["babel-loader"], exclude: /node_modules/},
            {
                test: /\.json?$/,
                loader: 'json-loader'
            },
        ]
    },
    plugins: [
        //new webpack.optimize.CommonsChunkPlugin('main', 'dist/main.bundle.js', Infinity),
    ],
    resolve: {
        root: path.resolve(__dirname),
        alias: {
            knockout: 'node_modules/knockout/build/output/knockout-latest',
        },
        extensions: ['', '.js']
    },
    eslint: {
        configFile: './.eslintrc',
        fix: true,
        //formatter: require("eslint-friendly-formatter"),
        formatter: require("eslint/lib/formatters/stylish"),
        emitError: false
    }
};
cfg.resolve.alias.knockout = 'node_modules/knockout/build/output/knockout-latest'

if (process.env.NODE_ENV === 'production') {
    cfg.plugins.push(new webpack.optimize.CommonsChunkPlugin('main', 'dist/main.bundle.js', Infinity));
}


module.exports = cfg;