import {AppViewModel} from './app.vm.js';
import ko from 'knockout';
var appTemplate = require('raw!./app.template.html');
let comp = { viewModel: AppViewModel, template: appTemplate };

ko.components.register("my-app", {
    viewModel: comp.viewModel,
    template: comp.template
});